package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.GameDAO;
import universite.angers.master.info.web.jee.bomberman.model.Game;

/**
 * API Game
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class GameAPI {

	private static APIRestStringToObject<Game> gameAPI;
	
	private GameAPI() {
		
	}
	
	public static synchronized APIRestStringToObject<Game> getInstance() {
		if(gameAPI == null) {
			gameAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, GameDAO.getInstance(), 
					new TypeToken<Game>() {}, new TypeToken<Collection<Game>>() {});
		}
		
		return gameAPI;
	}
}