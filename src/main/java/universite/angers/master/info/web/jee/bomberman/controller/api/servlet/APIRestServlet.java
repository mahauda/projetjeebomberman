package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.orm.util.UtilORM;

/**
 * API object
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class APIRestServlet<T> extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(APIRestServlet.class);
	private static final String FIELD_ID = "id";
	
	protected APIRestStringToObject<T> apiRest;
	
    public APIRestServlet(APIRestStringToObject<T> apiRest) {
       this.apiRest = apiRest;
    }

	/**
	 * Obtenir un objet ou une liste d'objets
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		String pathInfo = request.getPathInfo();
		LOG.debug("Path info do get : " + pathInfo);
		
		//Sans condition
		if(UtilORM.isNullOrEmpty(pathInfo)) {
			this.writeInResponse(response, this.apiRest.readAll());
		//Avec condition
		} else {
			Map<String, Object> where = this.getValueWhere(pathInfo);
			this.writeInResponse(response, this.apiRest.readAll(where));
		}
	}

	/**
	 * Créer un objet avec la méthode POST
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String body = this.getBodyRequest(request);
		LOG.debug("Body do post : " + body);
		
		String create = this.apiRest.create(body);
		LOG.debug("Create do post : " + body);
		
		this.writeInResponse(response, create);
	}

	/**
	 * Mise à jour d'un objet avec la méthode PUT
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String body = this.getBodyRequest(request);
		LOG.debug("Body do put : " + body);

		String update = this.apiRest.update(body);
		LOG.debug("Update do put : " + body);
		
		this.writeInResponse(response, update);
	}

	/**
	 * Suppression d'un objet avec la méthode DELETE
	 */
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String body = this.getBodyRequest(request);
		LOG.debug("Body do delete : " + body);
		
		String delete = this.apiRest.delete(body);
		LOG.debug("Delete do delete : " + body);
		
		this.writeInResponse(response, delete);
	}
	
	private Map<String, Object> getValueWhere(String pathInfo) {
		String[] pathInfoSplitted = pathInfo.split("/");
		LOG.debug("Path info splitted : " + Arrays.toString(pathInfoSplitted));
		
		if(pathInfoSplitted == null || pathInfoSplitted.length == 0) return Collections.emptyMap();
		
		//Si qu'une seule valeur est renseigné après le / dans ce cas on recherche par id
		if(pathInfoSplitted.length == 2) {
			String id = pathInfoSplitted[1];
			LOG.debug("ID : " + id);
			
			if(UtilORM.isNullOrEmpty(id)) return Collections.emptyMap();
			
			return Collections.singletonMap(FIELD_ID, id);
		}
		//Sinon on recherche la valeur par l'attribut passé dans l'URL
		else if(pathInfoSplitted.length == 3) {
			String column = pathInfoSplitted[1];
			LOG.debug("Column : " + column);
			
			if(UtilORM.isNullOrEmpty(column));
			
			String value = pathInfoSplitted[2];
			LOG.debug("Value : " + value);
			
			if(UtilORM.isNullOrEmpty(value)) return Collections.emptyMap();;
			
			Map<String, Object> where = Collections.singletonMap(column, value);
			
			String strObjects = this.apiRest.readAll(where);
			LOG.debug("Str objects : " + strObjects);
			
			if(UtilORM.isNullOrEmpty(strObjects)) return Collections.emptyMap();
			
			return Collections.singletonMap(column, value);
			
		} else return Collections.emptyMap();
	}
	
	private void writeInResponse(HttpServletResponse response, String message) {
        response.setContentType(this.apiRest.getContentType().getName());
        response.setCharacterEncoding(this.apiRest.getCharacterEncoding().getName());
        response.setStatus(this.apiRest.getResult().getCode());
        
		try {
			PrintWriter out = response.getWriter();
	        out.print(message);
	        out.flush();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	private String getBodyRequest(HttpServletRequest request) {
		try {
			 StringBuilder body = new StringBuilder();
			 BufferedReader reader = request.getReader();
			
			 String line = reader.readLine();
			 while (line != null) {
			      body.append(line);
			      line = reader.readLine();
			 }
			 
			 return body.toString();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return "";
		}
	}
}