package universite.angers.master.info.web.jee.bomberman.controller.form;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Formulaire qui permet de modifier les données relatives à l'utilisateur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */

public class UpdateAccountForm extends UserForm {

	private static final Logger LOG = Logger.getLogger(UpdateAccountForm.class);

	private static final String CHAMP_PSEUDO = "pseudo";
	private static final String CHAMP_PASS = "password";
	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_COUNTRY = "country";
	private static final String CHAMP_LASTNAME = "lastName";
	private static final String CHAMP_FIRSTNAME = "firstName";
	private static final String CHAMP_BIRTHDAY = "birthday";
	private static final String CHAMP_CONFIRMPASS = "confirmPassword";
	
	private String result;
	private Map<String, String> erreurs;

	public UpdateAccountForm() {
		this.erreurs = new HashMap<>();
	}

	/**
	 * Récupére la valeur d'un champ dans la requete
	 * 
	 * @param request
	 * @param fieldName
	 * @return
	 */
	private String getFieldValue(HttpServletRequest request, String fieldName) {
		String value = request.getParameter(fieldName);
		if (value == null || value.trim().length() == 0) {
			return null;
		} else {
			return value.trim();
		}
	}

	private void validationEmail(String email) throws Exception {
		if (email != null) {
			if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new Exception("Merci de saisir une adresse mail valide.");
			}
		} else {
			throw new Exception("Merci de saisir une adresse mail.");
		}
	}

	private void validationPseudo(String pseudo) throws Exception {
		if (pseudo != null && pseudo.length() < 3) {
			throw new Exception("Le pseudo doit contenir au moins 3 caractères.");
		}
	}

	private void validationfirstname(String firstName) throws Exception {
		if (firstName != null && firstName.length() < 3) {
			throw new Exception("Le prenom doit contenir au moins 3 caractères.");
		}
	}

	private void validationpassword(String password, String confirmpassword) throws Exception {
		if (password != null && password.trim().length() != 0 && confirmpassword != null
				&& confirmpassword.trim().length() != 0) {
			if (!password.equals(confirmpassword)) {
				throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
			} else if (password.trim().length() < 3) {
				throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
			}
		}
	}

	private void validationlastname(String lastName) throws Exception {
		if (lastName != null && lastName.length() < 3) {
			throw new Exception("Le nom doit contenir au moins 3 caractères.");
		}
	}

	/**
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		this.erreurs.put(champ, message);
	}

	public User updateAccountUser(HttpServletRequest request) {
		/* Récupération des champs du formulaire */
		User user = new User();
		
		String pseudo = getFieldValue(request, CHAMP_PSEUDO);
		LOG.debug("pseudo : " + pseudo);

		String password = getFieldValue(request, CHAMP_PASS);
		LOG.debug("password : " + password);

		String firstName = getFieldValue(request, CHAMP_FIRSTNAME);
		LOG.debug("firstName : " + firstName);

		String lastName = getFieldValue(request, CHAMP_LASTNAME);
		LOG.debug("lastName : " + lastName);

		String email = getFieldValue(request, CHAMP_EMAIL);
		LOG.debug("email : " + email);

		String confirmpassword = getFieldValue(request, CHAMP_CONFIRMPASS);
		LOG.debug("confirmpassword : " + confirmpassword);
		
		String birthday = getFieldValue(request, CHAMP_BIRTHDAY);
		LOG.debug("birth : " + birthday);
		
		try {
			Date birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthday);
			user.setBirthday(birthdate);
		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
		}

		String country = getFieldValue(request, CHAMP_COUNTRY);
		LOG.debug("country : " + country);		
		
		try {
			validationEmail(email);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		user.setEmail(email);

		try {
			validationPseudo(pseudo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_PSEUDO, e.getMessage());
		}
		user.setPseudo(pseudo);

		try {
			validationfirstname(firstName);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_FIRSTNAME, e.getMessage());
		}
		user.setFirstName(firstName);

		try {
			validationlastname(lastName);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_LASTNAME, e.getMessage());
		}
		user.setLastName(lastName);

		try {
			validationpassword(password, confirmpassword);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_PASS, e.getMessage());
		}
		user.setPassword(password);

		user.setCountry(country);
		
		LOG.debug("User :  " + user);

		if (this.erreurs.isEmpty()) {
			this.result = "Succès de la modification.";
			return user;
		} else {
			this.result = "Échec de la modification.";
			return null;
		}
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the erreurs
	 */
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	/**
	 * @param erreurs the erreurs to set
	 */
	public void setErreurs(Map<String, String> erreurs) {
		this.erreurs = erreurs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erreurs == null) ? 0 : erreurs.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateAccountForm other = (UpdateAccountForm) obj;
		if (erreurs == null) {
			if (other.erreurs != null)
				return false;
		} else if (!erreurs.equals(other.erreurs))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateAccountForm [resultat=" + result + ", erreurs=" + erreurs + "]";
	}
}
