package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import universite.angers.master.info.web.jee.bomberman.controller.dao.UserDAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.User;
import universite.angers.master.info.web.jee.bomberman.model.store.Store;
import universite.angers.master.info.web.jee.bomberman.model.store.Item;
import universite.angers.master.info.web.jee.bomberman.model.store.StoreFactory;

/**
 * Servlet qui permet d'afficher un item provenant du magasin
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/ItemServlet")
public class ItemServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ItemServlet.class);

	private static final String VUE = "/WEB-INF/jsp/item.jsp";
	private static final String ATT_ITEM = "item"; 
	private static final String ATT_ISPOSSIBLETOACESS = "isPossibleToAccess";	
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";
	private static final String URL_REDIRECTION = "http://localhost:8080/project-web-jee-bomberman/StoreServlet";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//On récupère le store
		Store store = StoreFactory.getInstance();
		
		//On récupère le compte
		Account account = (Account) request.getSession().getAttribute(ATT_SESSION_ACCOUNT);
		LOG.debug("Account : " + account);
		
		//On récupère l'utilisateur du compte
		User user = account.getUser();
		LOG.debug(user);		
		
		//On met à jour la boutique en fonction du compte
		store.updateAccount(account);
		
		//Récupérer l'identifiant de l'item dans la requete
		String itemIdentifiant = request.getParameter("itemIdentifiant");		
		LOG.debug("itemIdentifiant : " + itemIdentifiant);
		
		//On récupère l'item à partir de son identifiant
		Item item = (Item) store.getExperience(itemIdentifiant);
		LOG.debug("item : " + item);
		
		//Ajouter l'item dans la liste des items de l'utilisateur
		user.getItems().add(itemIdentifiant);	
		LOG.debug("user : " + user);
				
		//Mettre à jour le user
		UserDAO.getInstance().update(user);

		response.sendRedirect(URL_REDIRECTION);
	}
}
