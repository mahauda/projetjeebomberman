package universite.angers.master.info.web.jee.bomberman.model.store;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * Magasin qui regroupe un ensemble d'expériences à débloquer pour le jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Store {
	private static final Logger LOG = Logger.getLogger(Store.class);

	/**
	 * Les expériences à débloquer pour le jeu
	 */
	private Map<String, Experience> experiences;
		
	public Store() {
		this.experiences = new HashMap<>();	
	}	
	
	/**
	 * Ajouter une expérience (catégorie ou item) dans le magasin
	 * @param experience
	 * @return
	 */
	public boolean addExperience(Experience experience) {
		if(this.experiences.containsKey(experience.getIdentifiant())) return false;
		
		this.experiences.put(experience.getIdentifiant(), experience);
		return true;
	}

	/**
	 * Mettre à jour le compte dans l'ensemble des expériences afin de vérifier
	 * si le joueur peut accéder à l'offre
	 * @param account le compte qui contient le joueur
	 */
	public void updateAccount(Account account) {
		for(Experience experience : this.experiences.values()) {
			experience.setAccount(account);
		}
	}
	
	/**
	 * Affiche l'ensemble des expériences à débloquer
	 * @return
	 */
	public String display() {
		if(this.experiences.isEmpty()) return "";
		
		StringBuilder sb = new StringBuilder();

		for(Experience experience : this.experiences.values()) {
			sb.append(experience.display());
			sb.append("\n");
		}
		
		return sb.toString();
	}

	/**
	 * @return the experiences
	 */
	public Map<String, Experience> getExperiences() {
		return experiences;
	}

	/**
	 * @param experiences the experiences to set
	 */
	public void setExperiences(Map<String, Experience> experiences) {
		this.experiences = experiences;
	}
	
	/**
     * Récupérer une expérience par identifiant
     * @param identifiant
     * @return
     */
    public Experience getExperience(String identifiant) {
        Experience experience = null;

        for(Experience exp : this.experiences.values()) {
            if(exp.getIdentifiant().equals(identifiant))
                return exp;

            if(exp instanceof Category)
                experience = this.getExperience((Category)exp, identifiant);

            if(experience != null)
                return experience;
        }

        return experience;
    }
    
    /**
     * Récupérer une expérience dans une catégorie par identifant
     * @param cat
     * @param id
     * @return
     */
    private Experience getExperience(Category cat, String id) {
        Experience experience = null;

        for(Experience exp : cat.getExperiences().values()) {
            if(exp.getIdentifiant().equals(id))
                return exp;

            if(exp instanceof Category)
                experience = this.getExperience((Category)exp, id);

            if(experience != null)
                return experience;
        }

        return experience;
    }	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((experiences == null) ? 0 : experiences.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (experiences == null) {
			if (other.experiences != null)
				return false;
		} else if (!experiences.equals(other.experiences))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Store [experiences=" + experiences + "]";
	}
}
