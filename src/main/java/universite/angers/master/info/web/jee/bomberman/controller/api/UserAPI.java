package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.UserDAO;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class UserAPI {

	private static APIRestStringToObject<User> userAPI;

	private UserAPI() {

	}

	public static synchronized APIRestStringToObject<User> getInstance() {
		if (userAPI == null) {
			userAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, UserDAO.getInstance(),
					new TypeToken<User>() {}, new TypeToken<Collection<User>>() {});
		}

		return userAPI;
	}
}