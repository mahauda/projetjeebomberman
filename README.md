# Projet JEE Bomberman

Réalisé par :
- Théo MAHAUDA
- Anas TAGUENITI
- Mohamed OUHIRRA

Date : 09/04/2020\
Organisation : Master Informatique M1 à l'université d'Angers\
Version : 1.0\
Référence projet GitLab : https://gitlab.com/mahauda/projetjeebomberman

## Objectif

Permettre aux joueurs de télécharger le jeu Bomberman et de consulter l'état des parties joués (historique et classement).

## Architecture MVC

<img src="src/main/resources/images/MVC.png"/>

Afin de bien gérer et diviser le travail entre nous trois on a adopté cette architecture qui permet de bien séparer le code

    Modèle - Vue - Contrôlleur
    
Modèle : voir détail en bas

Vue : la couche représentation, sous forme des pages JSP (E.L. Expression Language)

Contôleur : Il reçoit les demandes de la couche de vue et les traite, y compris les validations nécessaires, présenter avec les servlet

### Modèles

<img src="src/main/resources/images/Model.png"/>

C’est la couche de données qui contient la logique applicative du système et représente également l’état de l’application.

**Account** modèle pour stocker les infos du compte

**User** modèle utilisateur

**Game** modèle des parties du jeu

**Rank** le rang d'utilisateur attribué selon son score total

### Vues

Pour tous les vues JSP dans notre projet on a choisi d'utiliser les Expressions Languages (EL), afin d'accéder plus simplement aux beans.

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

### BD

Pour stocker les données des utilisateurs, on a utilisé SQLite qui permet d'intégrer directement la base de données aux programmes.

**ContextListener** : une classe qui à pour rôle d'initialiser la base de données
        
    Supprimer la base de données, utile si il y a un changement dans le modèle de notre base :
	boolean delete = FactorySQLite.getInstance().getDataBase().delete();
	Créer une nouvelle schema de la base de données :
	boolean create = FactorySQLite.getInstance().getDataBase().create();
    Remplir la base de données avec des fausses données :
	this.populateDataBaseWithFakeData();

### DAO

Patron qui permet de chercher les données dans la BD, pour faciliter la tâche Théo MAHAUDA a développer un ORM fait maison èquivalent à Hibernate, pour tous ce qui CRUD et en plus des fonctionnalités spéciales pour notre projet.

### Filtre

Restreindre l'accès aux pages

**WebFilter** permet de vérifier les urls et laisser passer seulement les personnes connectées, à l'exception des pages suivantes (tout le monde peuvent y accéder) :

    path.startsWith("/HomeServlet")
    path.startsWith("/DeconnexionServlet")
	path.startsWith("/ConnexionServlet")
	path.startsWith("/InscriptionServlet")
	path.startsWith("/imports") 
	path.startsWith("/Semantic") : contient les fichiers css, js, images du framework utilier
	path.startsWith("/api") : accéder à l'API sans filtre

## Fonctionnalités implémentées

### Inscription d'un utilisateur

<img src="src/main/resources/images/Inscription.png"/>

**inscription.jsp** permet d'inscrire un nouveau utilisateur.

**InscriptionForm** vérifier la validité des champs saisies.

**InscriptionServlet** vérifier si l'utilisateur n'a pas déjà un compte, en cas d'un nouveau utilisateur, un compte sera créer avec l'utilisateur.

### Connexion d'un utilisateur

<img src="src/main/resources/images/Connexion.png"/>

La vue **connexion.jsp** permet aux utilisateurs de se connecter via leur pseudo et mot de passe, si l'utilisateur n'a pas de compte, le bouton s'inscrire permet de le faire.

**ConnexionForm** sert à vérifier les valeurs des champs saisies.

**ConnexionServlet** en cas d'erreurs dans les champs, on redirige l'utilisateur vers la vue connexion.jsp pour qu'il les corrige, sinon on récupère l'utilisateur adéquat, on créer la session et on lui dirige vers la page home.

### Déconnexion d'un utilisateur

<img src="src/main/resources/images/Deconnexion.png"/>

En cliquant sur le boutton Déconnexion en haut à droite, la pop-up s'ouvre, en attendant la confirmation ou l'annulation en cas d'erreur.

**DeconnexionServlet** permet de detruire la session en appelent session.invalidate();

### Mise à jour d'un compte utilisateur

<img src="src/main/resources/images/MajCompte.png"/>

La vue **updateAccount.jsp** permet au joueur de modifiers ses informations générales (nom, prenom, email, date de naissance ...).

**UpdateAccountForm** vérifier la validation des données avant de les enregistrer dans la base de données.

**UpdateAccountServlet** ici on instancie UpdateAccountForm pour vérifier c'est il y a des erreurs, si oui on redirige le joueur vers la vue de modification avec les erreurs sur les champs erronés, si non on met à jour les informations données par l'utilisateur.

### Suppression d'un compte utilisateur

Dans le menu à gauche, dernier lien **Supprimer mon compte**, permet de supprimer le compte de la base de données.

Dans la servlet **DeleteAccountServlet** on récupère le compte stockée dans la session, puis on appelle la fonction deleteAccount() du projet ORM pour supprimer le compte entièrement(compte, user, parties).

### Consultation d'un compte utilisateur

<img src="src/main/resources/images/Consultation.png"/>

la vue **account.jsp** permet au joueur de voir son compte, les informations générale en plus de Rang

**AccountServlet** redirige vers la vue account.jsp

### Téléchargement du jeu Bomberman

<img src="src/main/resources/images/Telechargement.png"/>

**Télécharger le jeu** le deuxième lien dans le menu à gauche nous permet de télécharger le jeu sous forme .jar

**DownloadServlet** préparer le fichier .jar pour le téléchargement 

### Consultation de l'historique des parties du joueur

<img src="src/main/resources/images/Historique.png"/>

La vue **game.jsp** permet au joueur de voir ses différentes parties (parties gagnées et parties perdues), avec la possibilité de trier les parties par score croissant ou décroissant.

Dans la servlet **GameServelet** on récupére la session et on redirige le joueur vers la vue game.jsp

### Consultation du classement général de tous les joueurs

<img src="src/main/resources/images/Classement.png"/>

La vue **ranking.jsp** offre la possiblité de voir un classement général avec deux manière : 

    classement par Score
    classement par Ratio victoire/défaite
    
**RankingServlet** permet de récupérer tous les comptes

### Boutique pour améliorer l'expérience du jeu

<img src="src/main/resources/images/Boutique.png"/>

La vue **store.jsp** permet aux utilisateurs d'acheter des éléments afin d'améliorer l'expérience du jeu
    
    Changer la couleur du Bomberman
    Débloquer un niveau, pour passer à un niveau élevé
    En plus de différentes Bonus :
        Speed : augmenter la vitesse du Bomberman
        Bonus vie
        Bonus bombe
        
Il suffit de cliquer sur le lien pour voir juste à droite le détail de l'item

En général si vous arrivez à voir le bouton débloquer dans le detail, alors vous avez le droit de le débloquer sinon il faut voir les conditions afin d'accéder à ce pouvoir.
    
Pour réaliser ça on a utilise le design pattern composant/composite. Voir détail ci-dessous.

**ItemServlet** met à jour l'utilisateur en lui rajoutant l'item débloquer.

**StoreServlet** vérifier si l'utilisateur à le droit de débloquer l'item via la fonction isPossibleToAccess().

<img src="src/main/resources/images/BoutiqueClasse.png"/>

### Design Pattern composant/composite

Ce patron permet de concevoir une structure arborescente
Dans notre projet :

    Composant : Experience
    Feuille : Item
    Composite : Category
    Store : Collection d'experience