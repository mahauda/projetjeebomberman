package universite.angers.master.info.web.jee.bomberman.controller.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.Game;

/**
 * DAO Game
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class GameDAO extends DAO<Game> {

	private static GameDAO gameDAO;

	private GameDAO() {
		super(FactorySQLite.getInstance(), Game.class);
	}

	public static synchronized GameDAO getInstance() {
		if (gameDAO == null)
			gameDAO = new GameDAO();

		return gameDAO;
	}
}
