package universite.angers.master.info.web.jee.bomberman.controller.listener;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.Logger;
import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.bd.jdbc.sqlite.SessionSQLite;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.Game;
import universite.angers.master.info.web.jee.bomberman.model.Rank;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Context qui permet d'initialiser la BD
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebListener
public class ContextListener implements ServletContextListener {
	
	/**
	 * Pour le débuggage : /media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Avance/TP/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/ProjectWebJEEBomberman/WEB-INF/bd
	 */
	
	private static final Logger LOG = Logger.getLogger(ContextListener.class);

	/**
	 * Les configs de la BD
	 */
	private static final boolean CREATE_BD = true;
	private static final String NAME_BD = "bomberman.db";
	private static final String PATH_BD = "WEB-INF/bd";
	private static final String PROVIDER_BD = "jdbc:sqlite";
	private static final String HOST_BD = "";
	private static final String PORT_BD = "";

	/**
	 *
	 * Le package qui contient tous les models
	 */
	private static final String PACKAGE_MODELS = "iut.angers.master.info.web.jee.bomberman.model";

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setRequestCharacterEncoding(StandardCharsets.UTF_8.name());
		sce.getServletContext().setResponseCharacterEncoding(StandardCharsets.UTF_8.name());

		String realPathBD = sce.getServletContext().getRealPath(PATH_BD);
		LOG.debug("Real path BD : " + realPathBD);
		
		// On crée la session avec JDBC
		SessionSQLite session = new SessionSQLite(CREATE_BD, NAME_BD, realPathBD, PROVIDER_BD, HOST_BD, PORT_BD);
		LOG.debug("Session : " + session);

		FactorySQLite.getInstance()
				.addSession(session)
				.addClassObject(Account.class)
				.addClassObject(Game.class)
				.addClassObject(User.class)
				.build();

		boolean open = FactorySQLite.getInstance().getDataBase().open();
		LOG.debug("Open : " + open);

//		boolean delete = FactorySQLite.getInstance().getDataBase().delete();
//		LOG.debug("Delete : " + delete);
//		
//		boolean create = FactorySQLite.getInstance().getDataBase().create();
//		LOG.debug("Create : " + create);
//
//		this.populateDataBaseWithFakeData();
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		boolean close = FactorySQLite.getInstance().getDataBase().close();
		LOG.debug("Close : " + close);
	}

	/**
	 * Peupler la BD avec des données fictives
	 */
	private void populateDataBaseWithFakeData() {
		Calendar.getInstance().set(1996, 01, 01);
		User anas = new User("Anas", "TAGUENITI", "anas.tagueniti@gmail.com", "Nana", "ATAGUENITI", "Maroc",
				Calendar.getInstance().getTime(), Rank.BRONZE);
		Game anasLevel1 = new Game(10, "Niveau 1", new Date(), false);
		Game anasLevel2 = new Game(20, "Niveau 2", new Date(), false);
		Game anasLevel3 = new Game(30, "Niveau 3", new Date(), true);

		Calendar.getInstance().set(1996, 02, 02);
		User mohamed = new User("Mohamed", "OUHIRRA", "mohamed.ouhirra@gmail.com", "Momo", "MOUHIRRA", "Maroc",
				Calendar.getInstance().getTime(), Rank.GOLD);
		Game mohamedLevel1 = new Game(10, "Niveau 1", new Date(), true);
		Game mohamedLevel2 = new Game(20, "Niveau 2", new Date(), true);
		Game mohamedLevel3 = new Game(30, "Niveau 3", new Date(), false);

		Calendar.getInstance().set(1996, 02, 12);
		User theo = new User("Théo", "MAHAUDA", "theo.mahauda@gmail.com", "Toto", "TMAHAUDA", "France",
				Calendar.getInstance().getTime(), Rank.CHAMPION);
		Game theoLevel1 = new Game(10, "Niveau 1", new Date(), false);
		Game theoLevel2 = new Game(20, "Niveau 2", new Date(), true);
		Game theoLevel3 = new Game(30, "Niveau 3", new Date(), false);

		Account accountAnas = new Account(anas);
		accountAnas.setGamesWon(Arrays.asList(anasLevel3));
		accountAnas.setGamesLost(Arrays.asList(anasLevel1, anasLevel2));
		
		Account accountMohamed = new Account(mohamed);
		accountMohamed.setGamesWon(Arrays.asList(mohamedLevel1, mohamedLevel2));
		accountMohamed.setGamesLost(Arrays.asList(mohamedLevel3));
		
		Account accountTheo = new Account(theo);
		accountTheo.setGamesWon(Arrays.asList(theoLevel2));
		accountTheo.setGamesLost(Arrays.asList(theoLevel1, theoLevel3));

		AccountDAO.getInstance().create(accountAnas);
		AccountDAO.getInstance().create(accountMohamed);
		AccountDAO.getInstance().create(accountTheo);
	}
}
