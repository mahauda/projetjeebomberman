package universite.angers.master.info.web.jee.bomberman.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

/**
 * Représente le compte du joueur avec les jeux gagnés et perdus
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@Table
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Son identifiant unique généré automatiquement par le constructeur avec l'aide d'un UUID
	 */
	@PrimaryKey
	private String id;
	
	/**
	 * L'utilisateur associé au compte
	 */
	@ForeignKey
	private User user;
	
	/**
	 * Les parties gagnées du joueur
	 */
	@ForeignKey
	private Collection<Game> gamesWon;
	
	/**
	 * Les parties perdus du joueur
	 */
	@ForeignKey
	private Collection<Game> gamesLost;
	
	/**
	 * Constructeur par défaut sans argument pour l'ORM
	 */
	public Account() {
		this(new User());
	}
	
	/**
	 * Constructeur avec arguments
	 * @param user
	 * @param gamesWon
	 * @param gamesLost
	 */
	public Account(User user) {
		this.id = user.getId();
		this.user = user;
		this.gamesWon = new ArrayList<>();
		this.gamesLost = new ArrayList<>();
	}

	/**
	 * Calcul le score total de toutes les parties jouées confondus (perdu et gagné)
	 * @return
	 */
	public int countScoreAllGames() {
		return this.countScoreGamesLost() + this.countScoreGamesWon();
	}
	
	/**
	 * Calcul le score total des parties remportés
	 * @return
	 */
	public int countScoreGamesWon() {
		return this.countScoreGames(this.gamesWon);
	}
	
	/**
	 * Calcul le score total des parties perdus
	 * @return
	 */
	public int countScoreGamesLost() {
		return this.countScoreGames(this.gamesLost);
	}
	
	/**
	 * Calcul le score total des parties jouées
	 * @return
	 */
	private int countScoreGames(Collection<Game> games) {
		int score = 0;
		
		for(Game game : games) {
			score += game.getScore();
		}
		
		return score;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the gamesWon
	 */
	public Collection<Game> getGamesWon() {
		return gamesWon;
	}

	/**
	 * @param gamesWon the gamesWon to set
	 */
	public void setGamesWon(Collection<Game> gamesWon) {
		this.gamesWon = gamesWon;
	}

	/**
	 * @return the gamesLost
	 */
	public Collection<Game> getGamesLost() {
		return gamesLost;
	}

	/**
	 * @param gamesLost the gamesLost to set
	 */
	public void setGamesLost(Collection<Game> gamesLost) {
		this.gamesLost = gamesLost;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gamesLost == null) ? 0 : gamesLost.hashCode());
		result = prime * result + ((gamesWon == null) ? 0 : gamesWon.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (gamesLost == null) {
			if (other.gamesLost != null)
				return false;
		} else if (!gamesLost.equals(other.gamesLost))
			return false;
		if (gamesWon == null) {
			if (other.gamesWon != null)
				return false;
		} else if (!gamesWon.equals(other.gamesWon))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Accout [id=" + id + ", user=" + user + ", gamesWon=" + gamesWon + ", gamesLost=" + gamesLost + "]";
	}
}
