package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.controller.form.UpdateAccountForm;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.User;

import java.io.IOException;

/**
 * Servlet qui permet de mettre à jour un utilisateur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/UpdateAccountServlet")
public class UpdateAccountServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(UpdateAccountServlet.class);

	private static final String ATT_USER = "user";
	private static final String ATT_FORM = "form";
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";
	private static final String VUE = "/WEB-INF/jsp/updateAccount.jsp";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Affichage de la page de modification */
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
		UpdateAccountForm form = new UpdateAccountForm();

		/* Traitement de la requête et récupération du bean en résultant */
		User userUpdated = form.updateAccountUser(request);
		LOG.debug("User updated =  " + userUpdated);
		
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		LOG.debug("session :" + session);
		
		/**
		 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean Utilisateur à
		 * la session, sinon suppression du bean de la session.
		 */
		if (form.getErreurs().isEmpty()) {
			Account session_account = (Account) session.getAttribute(ATT_SESSION_ACCOUNT);
			LOG.debug("Objet stocké dans la session :" + session_account);
			
			//On recupere le user dans le compte 
			User user = session_account.getUser();
			
			//On mets a jour le user en utilisant la fonction updateuser de User 
			user.updateUser(userUpdated);
			
			// On met a jour le compte sur la base de donnees 
			boolean isUpdated = AccountDAO.getInstance().update(session_account);
			LOG.debug("isUpdated : " + isUpdated);
			
			//Si tout ce passe bien alors on met à jour la session
			if (isUpdated) {
				session.setAttribute(ATT_SESSION_ACCOUNT, session_account);
			}
			else {
				session.setAttribute(ATT_SESSION_ACCOUNT, null);
			}
		} else {
			session.setAttribute(ATT_SESSION_ACCOUNT, null);
		}
		
		/* Stockage du formulaire et du bean dans l'objet request */
		request.setAttribute(ATT_FORM, form);
		request.setAttribute(ATT_USER, userUpdated);
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

}
