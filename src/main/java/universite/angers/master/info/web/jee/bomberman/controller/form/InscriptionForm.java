package universite.angers.master.info.web.jee.bomberman.controller.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Formulaire qui permet de vérifier les données de la requete issu de la page
 * d'inscription
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class InscriptionForm extends UserForm {

	private static final Logger LOG = Logger.getLogger(InscriptionForm.class);

	private static final String CHAMP_FIRSTNAME = "firstName";
	private static final String CHAMP_LASTNAME = "lastName";
	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_CONFIRMEMAIL = "email_confirme";
	private static final String CHAMP_PSEUDO = "pseudo";
	private static final String CHAMP_PASS = "password";
	private static final String CHAMP_CONFIRMPASS = "password_confirme";
	private static final String CHAMP_DATE = "birthday";
	private static final String CHAMP_COUNTRY = "country";

	/**
	 * Chaine resultat et Map erreurs
	 */
	private String result;
	private Map<String, String> erreurs;

	public InscriptionForm() {
		this.erreurs = new HashMap<>();
	}

	/**
	 * Les methodes de validation des champs de l'utilisateur
	 */

	private void validationEmail(String email) throws Exception {
		if (email != null) {
			if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new Exception("Merci de saisir une adresse mail valide.");
			}
		} else {
			throw new Exception("Merci de saisir une adresse mail.");
		}
	}

	private void validationPseudo(String pseudo) throws Exception {
		if (pseudo != null && pseudo.length() < 3) {
			throw new Exception("Le pseudo doit contenir au moins 3 caractères.");
		}
	}

	private void validationfirstname(String firstName) throws Exception {
		if (firstName != null && firstName.length() < 3) {
			throw new Exception("Le prenom doit contenir au moins 3 caractères.");
		}
	}

	private void validationpassword(String password, String confirmpassword) throws Exception {
		if (password != null && password.trim().length() != 0 && confirmpassword != null
				&& confirmpassword.trim().length() != 0) {
			if (!password.equals(confirmpassword)) {
				throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
			} else if (password.trim().length() < 3) {
				throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
			}
		}
	}

	private void validationlastname(String lastName) throws Exception {
		if (lastName != null && lastName.length() < 3) {
			throw new Exception("Le nom doit contenir au moins 3 caractères.");
		}
	}

	/**
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/**
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getFieldValue(HttpServletRequest request, String fieldName) {
		String value = request.getParameter(fieldName);
		if (value == null || value.trim().length() == 0) {
			return null;
		} else {
			return value.trim();
		}
	}

	public User registerUser(HttpServletRequest request) {
		String pseudo = getFieldValue(request, CHAMP_PSEUDO);
		LOG.debug("pseudo : " + pseudo);

		String password = getFieldValue(request, CHAMP_PASS);
		LOG.debug("password : " + password);

		String firstName = getFieldValue(request, CHAMP_FIRSTNAME);
		LOG.debug("firstName : " + firstName);

		String lastName = getFieldValue(request, CHAMP_LASTNAME);
		LOG.debug("lastName : " + lastName);

		String email = getFieldValue(request, CHAMP_EMAIL);
		LOG.debug("email : " + email);

		String date = getFieldValue(request, CHAMP_DATE);
		LOG.debug("date : " + date);

		String confirmemail = getFieldValue(request, CHAMP_CONFIRMEMAIL);
		LOG.debug("confirmemail : " + confirmemail);

		String confirmpassword = getFieldValue(request, CHAMP_CONFIRMPASS);
		LOG.debug("confirmpassword : " + confirmpassword);

		String country = getFieldValue(request, CHAMP_COUNTRY);
		LOG.debug("country : " + country);

		User user = new User();

		try {
			validationEmail(email);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_EMAIL, e.getMessage());
		}

		user.setEmail(email);

		try {
			validationPseudo(pseudo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_PSEUDO, e.getMessage());
		}
		user.setPseudo(pseudo);

		try {
			validationfirstname(firstName);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_FIRSTNAME, e.getMessage());

		}
		user.setFirstName(firstName);

		try {
			validationlastname(lastName);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_LASTNAME, e.getMessage());
		}
		user.setLastName(lastName);

		try {
			validationpassword(password, confirmpassword);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setErreur(CHAMP_PASS, e.getMessage());
		}
		user.setPassword(password);

		/**
		 * Faire des vérifs de country et de birthday
		 */

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			// format() method Formats a Date into a date/time string.
			Date d1 = df.parse(date);
			user.setBirthday(d1);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		user.setCountry(country);

		if (erreurs.isEmpty()) {
			result = "Succès de l'inscription.";
		} else {
			result = "Échec de l'inscription.";
		}

		return user;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the erreurs
	 */
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	/**
	 * @param erreurs the erreurs to set
	 */
	public void setErreurs(Map<String, String> erreurs) {
		this.erreurs = erreurs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erreurs == null) ? 0 : erreurs.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InscriptionForm other = (InscriptionForm) obj;
		if (erreurs == null) {
			if (other.erreurs != null)
				return false;
		} else if (!erreurs.equals(other.erreurs))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InscriptionForm [result=" + result + ", erreurs=" + erreurs + "]";
	}
}
