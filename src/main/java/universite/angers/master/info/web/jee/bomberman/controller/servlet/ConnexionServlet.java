package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.controller.dao.UserDAO;
import universite.angers.master.info.web.jee.bomberman.controller.form.ConnexionForm;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.User;
import java.io.IOException;
import java.io.Serializable;

/**
 * Servlet qui permet de connecter un utilisateur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/ConnexionServlet")
public class ConnexionServlet extends HttpServlet implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ConnexionServlet.class);

	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";
	private static final String ATT_USER = "user";
	private static final String ATT_FORM = "form";
	private static final String ATT_SESSION_USER = "sessionUtilisateur";
	private static final String VUE = "/WEB-INF/jsp/connexion.jsp";
	private static final String VUE_USER = "/WEB-INF/jsp/user.jsp";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* Préparation de l'objet formulaire */
		ConnexionForm form = new ConnexionForm();
		LOG.debug("ConnexionForm : " + form);

		/* Traitement de la requête et récupération du bean en résultant */
		User user = form.connectUser(request);
		LOG.debug("User : " + user);

		/* Verification de l'exsistence aupres de la bdd */
		// Recuperation du user de la bdd

		User userConnected = UserDAO.getInstance().getUserByPseudo(user.getPseudo());
		LOG.debug("User connected : " + userConnected);

		/* Stockage du formulaire et du bean dans l'objet request */
		request.setAttribute(ATT_FORM, form);
		request.setAttribute(ATT_USER, user);
		
		//Si on a récupérer l'utilisateur
		if (userConnected != null) {
			
			//On récupère le compte associé à l'utilisateur
			Account account = AccountDAO.getInstance().getAccountByUser(userConnected);
			LOG.debug("Account : " + account);

			// recuperation du mot de passe de la bdd
			String password = account.getUser().getPassword();
			LOG.debug("Password : " + password);

			boolean checkPassWord = form.checkPassWord(password, user.getPassword());
			LOG.debug("Password checked : " + checkPassWord);

			form.checkResultat();

			//Si le mot de passe est correct
			if (checkPassWord) {

				/* Récupération de la session depuis la requête */
				HttpSession session = request.getSession();

				/*
				 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean Utilisateur à
				 * la session, sinon suppression du bean de la session.
				 */
				session.setAttribute(ATT_SESSION_ACCOUNT, account);
				LOG.debug(session);

				/*
				 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean Utilisateur à
				 * la session, sinon suppression du bean de la session.
				 */
				session.setAttribute(ATT_SESSION_USER, userConnected);

				this.getServletContext().getRequestDispatcher(VUE_USER).forward(request, response);
			} else {
				this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
			}
		} else {
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		}

		// if (password == null) return;

		// if(JEEUtil.isNullOrEmpty(password)) return;

		// boolean verification = CryptPasswordUtil.checkPassword(user.getPassword(),
		// password);
		// LOG.debug("mot de passe --------- " + password);

		// recuperation du mot de passe de la bdd

		// LOG.debug(password);

		// if(JEEUtil.isNullOrEmpty(password)) return;
		// if(verification){}
	}
}