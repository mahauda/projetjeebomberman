<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  		<link href="imports/Bomberman-icon.png" rel="icon" sizes="128x128">
		<title>Connexion</title>
		
		 
		<link rel="stylesheet" type="text/css" href="Semantic/components/reset.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/site.css">
		
		<link rel="stylesheet" type="text/css" href="Semantic/components/container.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/grid.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/header.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/image.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/menu.css">
		
		<link rel="stylesheet" type="text/css" href="Semantic/components/divider.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/segment.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/form.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/input.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/button.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/list.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/message.css">
		<link rel="stylesheet" type="text/css" href="Semantic/components/icon.css">
	

		<style type="text/css">
		  body {
		    background-color: #DADADA;
		  }
		  body > .grid {
		    height: 100%;
		  }
		  .image {
		    margin-top: -100px;
		  }
		  .column {
		    max-width: 450px;
		  }
		</style>
<!--   <script>
  $(document).ready(function() {
/*       $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your e-mail'
                },
                {
                  type   : 'email',
                  prompt : 'Please enter a valid e-mail'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Your password must be at least 6 characters'
                }
              ]
            }
          }
        })
      ; */
    })
  ;
  </script> -->
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">      
      <div class="content">
        Connectez-vous
      </div>
    </h2>
    <form class="ui large form" method="post" action="ConnexionServlet" autocomplete="off">
      <div class="ui stacked segment left aligned">      		
	    	<c:choose>
			  <c:when test="${!empty form.erreurs}">
				<div class="ui message">
				    <div class="header"><c:out value="${form.resultat}"/></div>
				    <ul class="list">				    					    					      							    				    
				      	<c:forEach var="entry" items="${form.erreurs}">							  
							<li><c:out value="${entry.value}"/></li>
						</c:forEach>
				    </ul>
				 </div>    
			  </c:when>
		   	</c:choose>	
        <div class="field">
		  <label>Pseudo</label>
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="pseudo" value="<c:out value="${user.pseudo}"/>" required placeholder="Pseudo" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" required>
          </div>
        </div>
        <div class="field">
			<label>Mot de passe</label>
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" required="required" placeholder="Password" required style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
          </div>
        </div>
        <input type="submit" value="Se connecter" class="ui fluid large teal button" />
      </div>

      <div class="ui error message"></div>

    </form>
	 <form method="get" action="InscriptionServlet">
    <div class="ui message">
      Vous n'êtes pas inscrit? <input type="submit" value="S'inscrire" class="ui fluid large teal button" />
    </div>
    </form>
  </div>
</div>
</body></html>
