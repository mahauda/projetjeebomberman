package universite.angers.master.info.web.jee.bomberman.model.store;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.model.Rank;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Item qui permet d'améliorer l'expérience du jeu en débloquant 
 * des contenus supplémentaires pour le jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Item extends Experience {

	private static final Logger LOG = Logger.getLogger(Item.class);
	
	/**
	 * Le prix pour acheter cet item
	 */
	private int price;
	
	/**
	 * Les rangs autorisés pour débloquer l'item
	 */
	private Collection<Rank> ranks;

	public Item() {
		this("", "", "", "", "");
	}
	
	public Item(String identifiant, String name, String condition, String description, String pathPicture) {
		super(identifiant, name, condition, description, pathPicture);		
		this.ranks = new ArrayList<>();
	}


	@Override
	public boolean isPossibleToAccess() {
		//1) Il faut que l'utilisateur ne l'est pas en sa possesion
		User user = this.account.getUser();
		LOG.debug("User : " + user);
		
		if(user == null) return false;
		
		boolean noItemRespected = !user.getItems().contains(this.identifiant);
		LOG.debug("Item non possédé : " + noItemRespected);
		
		//2) Il faut qu'il est le nombre requis de score
		
		int score = this.account.countScoreAllGames();
		LOG.debug("Score : " + score);
		
		boolean priceRespected = this.account.countScoreAllGames() >= this.price;
		LOG.debug("Prix respecté : " + priceRespected);
		
		//3) Il faut qu'il est le rang dans la liste
		
		boolean rankRespected = this.ranks.contains(user.getRank());
		LOG.debug("Rang respecté : " + rankRespected);
		
		return noItemRespected && priceRespected && rankRespected;
	}
	
	@Override
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"title\">");
		sb.append("<a href=\"http://localhost:8080/project-web-jee-bomberman/StoreServlet?item="+ this.identifiant +"\">");
		sb.append(this.name);
		sb.append("</a>");
		sb.append("</div>");
	                          
        return sb.toString();
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the ranks
	 */
	public Collection<Rank> getRanks() {
		return ranks;
	}

	/**
	 * @param ranks the ranks to set
	 */
	public void setRanks(Collection<Rank> ranks) {
		this.ranks = ranks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + price;
		result = prime * result + ((ranks == null) ? 0 : ranks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (price != other.price)
			return false;
		if (ranks == null) {
			if (other.ranks != null)
				return false;
		} else if (!ranks.equals(other.ranks))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Item [price=" + price + ", ranks=" + ranks + ", account=" + account + ", identifiant=" + identifiant
				+ ", name=" + name + ", condition=" + condition + ", description=" + description + ", pathPicture="
				+ pathPicture + "]";
	}
}
