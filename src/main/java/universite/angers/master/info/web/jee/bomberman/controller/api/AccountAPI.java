package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * API account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AccountAPI {

	private static APIRestStringToObject<Account> accountAPI;
	
	private AccountAPI() {
		
	}
	
	public static synchronized APIRestStringToObject<Account> getInstance() {
		if(accountAPI == null) {
			accountAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, AccountDAO.getInstance(), 
					new TypeToken<Account>() {}, new TypeToken<Collection<Account>>() {});
		}
		
		return accountAPI;
	}
}