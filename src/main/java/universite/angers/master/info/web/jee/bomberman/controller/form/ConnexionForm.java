package universite.angers.master.info.web.jee.bomberman.controller.form;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Formulaire qui permet de vérifier les données de la requete issu de la page
 * de connexion
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ConnexionForm extends UserForm {

	private static final Logger LOG = Logger.getLogger(ConnexionForm.class);

	private static final String CHAMP_PSEUDO = "pseudo";
	private static final String CHAMP_PASS = "password";

	private String resultat;
	private Map<String, String> erreurs;

	public ConnexionForm() {
		this.erreurs = new HashMap<>();
	}

	/**
	 * Récupére la valeur d'un champ dans la requete
	 * 
	 * @param request
	 * @param fieldName
	 * @return
	 */
	private String getFieldValue(HttpServletRequest request, String fieldName) {
		String value = request.getParameter(fieldName);
		if (value == null || value.trim().length() == 0) {
			return null;
		} else {
			return value.trim();
		}
	}

	/**
	 * Rechercher l'utilisateur
	 * 
	 * @param request
	 * @return
	 */
	public User connectUser(HttpServletRequest request) {
		/* Récupération des champs du formulaire */
		String pseudo = getFieldValue(request, CHAMP_PSEUDO);
		LOG.debug("pseudo    :" + pseudo);
		String password = getFieldValue(request, CHAMP_PASS);
		LOG.debug("password    :" + password);
		User user = new User();

		/* Validation du champ email. */
		try {
			validationPseudo(pseudo);
		} catch (Exception e) {
			setErreur(CHAMP_PSEUDO, e.getMessage());
		}

		user.setPseudo(pseudo);

		/* Validation du champ mot de passe. */
		try {
			validationMotDePasse(password);
		} catch (Exception e) {
			setErreur(CHAMP_PASS, e.getMessage());
		}

		user.setPassword(password);

		LOG.debug("user    :" + user);

		return user;
	}

	public boolean checkPassWord(String passUser, String passForm) {
		/* Validation du mot de passe. */
		try {
			equalsPassWord(passUser, passForm);
		} catch (Exception e) {
			setErreur(CHAMP_PASS, e.getMessage());
		}

		return passUser.equals(passForm);

	}

	public void checkResultat() {
		/* Initialisation du résultat global de la validation. */
		if (this.erreurs.isEmpty()) {
			this.resultat = "Succès de la connexion.";
		} else {
			this.resultat = "Échec de la connexion.";
		}
	}

	/**
	 * Vérifier si le mot de passe est bon
	 */
	public void equalsPassWord(String passUser, String passForm) throws Exception {
		if (!passUser.equals(passForm)) {
			throw new Exception("Mot de passe erroné");
		}
	}

	/**
	 * Valide le pseudo saisie.
	 */
	private void validationPseudo(String pseudo) throws Exception {
		if (pseudo != null && pseudo.length() < 3) {
			throw new Exception("Le pseudo doit contenir au moins 3 caractères.");
		}
		if (!existPseudoBdd(pseudo)) {
			throw new Exception("Vous n'etes pas inscris");

		}
	}

	/**
	 * Valide le mot de passe saisi.
	 */
	private void validationMotDePasse(String motDePasse) throws Exception {
		if (motDePasse != null) {
			if (motDePasse.length() < 6) {
				throw new Exception("Le mot de passe doit contenir au moins 6 caractères.");
			}
		} else {
			throw new Exception("Merci de saisir votre mot de passe.");
		}
	}

	/**
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/**
	 * @return the resultat
	 */
	public String getResultat() {
		return resultat;
	}

	/**
	 * @param resultat the resultat to set
	 */
	public void setResultat(String resultat) {
		this.resultat = resultat;
	}

	/**
	 * @return the erreurs
	 */
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	/**
	 * @param erreurs the erreurs to set
	 */
	public void setErreurs(Map<String, String> erreurs) {
		this.erreurs = erreurs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erreurs == null) ? 0 : erreurs.hashCode());
		result = prime * result + ((resultat == null) ? 0 : resultat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConnexionForm other = (ConnexionForm) obj;
		if (erreurs == null) {
			if (other.erreurs != null)
				return false;
		} else if (!erreurs.equals(other.erreurs))
			return false;
		if (resultat == null) {
			if (other.resultat != null)
				return false;
		} else if (!resultat.equals(other.resultat))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConnexionForm [resultat=" + resultat + ", erreurs=" + erreurs + "]";
	}
}
